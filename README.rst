Test Singularity Exec GeneFlow App
==================================

Version: 0.1

This GeneFlow app tests singularity image execution.

Inputs
------

1. input: Dummy input file.

Parameters
----------

1. string: String to print. Default: 'hello'.
 
2. output: Output text file.

